//Creación de las variable en la que se imprimirá la respuesta del servidor a través de AJAX y de la variable con la ruta del archivo JSON
var respuesta = document.getElementById('slider');
var datos = "api/slides.json";
//Inicialización de las variables que van a contener el código HTML del slider
var htmlcode = "";
var botones = "";
botones += "<div class='buttons'>";
botones += "<div class='next' onclick='cambiarSlide()'>&#10095;</div>";
botones += "<div class='prev'>&#10094;</div>";
botones += "</div>";
simBD();

//Función AJAX para simular una base de datos en la carga del slider
function simBD() {

    var slider = new XMLHttpRequest();
    slider.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var myObj = JSON.parse(this.responseText);
        //Bucle para recorrer el archivo JSON y generar el código HTML
        for (var i = 0; i < myObj.data.length; i++) {
          htmlcode += "<div class='slide fade' id='slide"+myObj.data[i].id+"'>";
          htmlcode += "<img src='"+myObj.data[i].bg_image+"' alt='Foto "+myObj.data[i].id+" del slider'>";
          if (myObj.data[i].id <= 2) {
            htmlcode += "<div class='overText'>";
          }
          else {
            htmlcode += "<div class='overText2'>";
          }
          htmlcode += "<h2 class='titulo'>"+myObj.data[i].title+"</h2>";
          htmlcode += "<a class='button' href='"+myObj.data[i].button_link+"'>"+myObj.data[i].button_text+"</a>";
          htmlcode += "</div>";
          htmlcode += "</div>";
          respuesta.innerHTML = htmlcode;
        }
          respuesta.innerHTML += botones;
          var slideIndex = 0; //Inicialización del índice de los slides
          mostrarSlide();

          function mostrarSlide() {
              var i;
              var slides = document.getElementsByClassName("slide");
              for (i = 0; i < slides.length; i++) {
                  slides[i].style.display = "none";//Oculta todas las imágenes
              }
              slideIndex++;
              if (slideIndex > slides.length) {
                  slideIndex = 1;
              }
              slides[slideIndex-1].style.display = "block"; //Muestra la imagen que corresponda
              setTimeout(mostrarSlide, 7000); //Cambia la imagen de fondo cada 7 segundos*/
          }
          
      }
    };
    slider.open("GET", datos, true);
    slider.send();

}

 //Creación de las variable en la que se imprimirá la respuesta del servidor a través de AJAX y de la variable con la ruta del archivo JSON
 var catalog = document.getElementById("products");
 var archivoJson = "api/products.json";
 var htmlcode2 = "";
 prodBD();

 //Función AJAX para simular una base de datos en la carga de los productos
 function prodBD() {

    var productos = new XMLHttpRequest();
    productos.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var listaProd = JSON.parse(this.responseText);
        //Bucle para recorrer el archivo JSON y generar el código HTML
        for (var i = 0; i <= 3; i++) {
          htmlcode2 += "<div class='item"+listaProd.data[i].id+"'>";
          htmlcode2 += "<div><img src='"+listaProd.data[i].image+"' alt='Foto "+listaProd.data[i].name+"'></div>";
          htmlcode2 += "<div class='datos'>";
          htmlcode2 += "<div class='prodName'><span>"+listaProd.data[i].name+"</span></div><div class='prodPrice'><div>"+listaProd.data[i].price+"</div></div>";
          htmlcode2 += "<div class='compra'><a class='button' href='"+listaProd.data[i].button_link+"'>"+listaProd.data[i].button_text+"</a></div>";
          htmlcode2 += "</div>";
          htmlcode2 += "</div>";
        }
        htmlcode2 += "<div class='item5'><img src='./resources/cta/cta1.jpg'></div>";
        for (var i = 4; i <= 6; i++) {
          listaProd.data[i].id++;
          htmlcode2 += "<div class='item"+listaProd.data[i].id+"'>";
          htmlcode2 += "<div><img src='"+listaProd.data[i].image+"' alt='Foto "+listaProd.data[i].name+"'></div>";
          htmlcode2 += "<div class='datos'>";
          htmlcode2 += "<div class='prodName'><span>"+listaProd.data[i].name+"</span></div><div class='prodPrice'><div>"+listaProd.data[i].price+"</div></div>";
          htmlcode2 += "<div class='compra'><a class='button' href='"+listaProd.data[i].button_link+"'>"+listaProd.data[i].button_text+"</a></div>";
          htmlcode2 += "</div>";
          htmlcode2 += "</div>";
        }
        htmlcode2 += "<div class='item9'><img src='./resources/cta/cta2.jpg'></div>";
        listaProd.data[7].id+=2;
        htmlcode2 += "<div class='item"+listaProd.data[7].id+"'>";
        htmlcode2 += "<div><img src='"+listaProd.data[7].image+"' alt='Foto "+listaProd.data[7].name+"'></div>";
        htmlcode2 += "<div class='datos'>";
        htmlcode2 += "<div class='prodName'><span>"+listaProd.data[7].name+"</span></div><div class='prodPrice'><div>"+listaProd.data[7].price+"</div></div>";
        htmlcode2 += "<div class='compra'><a class='button' href='"+listaProd.data[7].button_link+"'>"+listaProd.data[7].button_text+"</a></div>";
        htmlcode2 += "</div>";
        htmlcode2 += "</div>";
        catalog.innerHTML = htmlcode2;
      }
    };
    productos.open("GET", archivoJson, true);
    productos.send();

 }

 //Creación de las variable en la que se imprimirá la respuesta del servidor a través de AJAX
 var catalog2 = document.getElementById('load');
 var htmlcode3 = "";

 //Función AJAX para simular una base de datos en la carga de los productos al hace click sobre el botón de carga
 function prodBD2() {

  var loadBtn = document.getElementById('loadBtn');
  var productos = new XMLHttpRequest();
  productos.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var listaProd = JSON.parse(this.responseText);
      //Bucle para recorrer el archivo JSON y generar el código HTML
      for (var i = 0; i <= 3; i++) {
        htmlcode3 += "<div class='item"+listaProd.data[i].id+"'>";
        htmlcode3 += "<div><img src='"+listaProd.data[i].image+"' alt='Foto "+listaProd.data[i].name+"'></div>";
        htmlcode3 += "<div class='datos'>";
        htmlcode3 += "<div class='prodName'><span>"+listaProd.data[i].name+"</span></div><div class='prodPrice'><div>"+listaProd.data[i].price+"</div></div>";
        htmlcode3 += "<div class='compra'><a class='button' href='"+listaProd.data[i].button_link+"'>"+listaProd.data[i].button_text+"</a></div>";
        htmlcode3 += "</div>";
        htmlcode3 += "</div>";
      }
      htmlcode3 += "<div class='item5'><img src='./resources/cta/cta1.jpg'></div>";
      for (var i = 4; i <= 6; i++) {
        listaProd.data[i].id++;
        htmlcode3 += "<div class='item"+listaProd.data[i].id+"'>";
        htmlcode3 += "<div><img src='"+listaProd.data[i].image+"' alt='Foto "+listaProd.data[i].name+"'></div>";
        htmlcode3 += "<div class='datos'>";
        htmlcode3 += "<div class='prodName'><span>"+listaProd.data[i].name+"</span></div><div class='prodPrice'><div>"+listaProd.data[i].price+"</div></div>";
        htmlcode3 += "<div class='compra'><a class='button' href='"+listaProd.data[i].button_link+"'>"+listaProd.data[i].button_text+"</a></div>";
        htmlcode3 += "</div>";
        htmlcode3 += "</div>";
      }
      htmlcode3 += "<div class='item9'><img src='./resources/cta/cta2.jpg'></div>";
      listaProd.data[7].id+=2;
      htmlcode3 += "<div class='item"+listaProd.data[7].id+"'>";
      htmlcode3 += "<div><img src='"+listaProd.data[7].image+"' alt='Foto "+listaProd.data[7].name+"'></div>";
      htmlcode3 += "<div class='datos'>";
      htmlcode3 += "<div class='prodName'><span>"+listaProd.data[7].name+"</span></div><div class='prodPrice'><div>"+listaProd.data[7].price+"</div></div>";
      htmlcode3 += "<div class='compra'><a class='button' href='"+listaProd.data[7].button_link+"'>"+listaProd.data[7].button_text+"</a></div>";
      htmlcode3 += "</div>";
      htmlcode3 += "</div>";
      catalog2.innerHTML = htmlcode2;
    }
  };
  productos.open("GET", archivoJson, true);
  productos.send();
  loadBtn.style.display = "none";

}

